// Copyright (c) 2023 Boomi, Inc.
package com.boomi.salesforce.rest.authenticator;

/**
 * Representation of the available authentication types for this connector: User credentials and OAuth 2.0
 */
public enum AuthenticationType {
    USER_CREDENTIALS, OAUTH_2
}
